#!/bin/bash

path_ls()
{
	unset IFS
	echo $PATH | tr ':' '\n'
}

_path_add_check()
{
	add="$1"

	[ ! -e "$add" ] && echo "Could not add \"$add\" to \$PATH because it does not exist" && return -1
	[ ! -d "$add" ] && echo "Could not add \"$add\" to \$PATH because is not a directory" && return -1

	if path_includes "$add"; then
		echo "Directory \"$add\" already in \$PATH"
		echo
		echo "# -- definition of \$PATH ="
		path_ls
		echo "# --"
		return -1
	fi
}

path_prepend()
{
	add="$1"

	_path_add_check "$add"

	export PATH="$add:$PATH"
}

path_append()
{
	add="$1"

	_path_add_check "$add"

	export PATH="$PATH:$add"
}

path_includes()
{
	check="$1"

	IFS=':'
	for dir in $PATH;
	do
		if [ "$dir" = "$check" ] ; then
			unset IFS
			return 0;
		fi
	done
	unset IFS

	return -1;
}
