# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc) for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

BASHD="${HOME}/.bash.d"

[ -f "${BASHD}/functions.sh" ] && source "${BASHD}/functions.sh"
[ -f "${BASHD}/git-prompt.sh" ] && source "${BASHD}/git-prompt.sh"

# ──━━┥ Allow to re-initialize $PATH ┝━━──

if [ -z "$INITIAL_PATH" ] ; then
	export INITIAL_PATH="$PATH"
else
	export PATH="$INITIAL_PATH"
fi

# Include default ubuntu bashrc
[ -f $HOME/.bashrc_ubuntu_default ] && source $HOME/.bashrc_ubuntu_default

# ──━━┥ KEYCHAIN ┝━━──
KEYCHAIN="$(command -v keychain 2>&1)"

if [ ! -z "${KEYCHAIN}" ] ; then

	keychain id_rsa
	[ -z "$HOSTNAME" ] && HOSTNAME=`uname -n`
	[ -f $HOME/.keychain/$HOSTNAME-sh ] && source $HOME/.keychain/$HOSTNAME-sh
	[ -f $HOME/.keychain/$HOSTNAME-sh-gpg ] && source $HOME/.keychain/$HOSTNAME-sh-gpg

else
	echo "+++ keychain is not installed!!"
fi

# ──━━┥ DOTFILE MANAGEMENT ┝━━──
alias dot="git --work-tree=$HOME --git-dir=$HOME/src/dotfiles.git"

# ──━━┥ GENERAL PATH MANAGEMENT ┝━━──
[ -d $HOME/local/bin ] && path_prepend $HOME/local/bin
if ! path_includes $HOME/bin; then
	path_prepend $HOME/bin
fi

# ──━━┥ COMMON COMMAND ALIASES ┝━━──
alias vi="vim"
alias vimrc="vim $HOME/.vim/vimrc"
alias ls="ls -l --color"

export EDITOR=vim

# ──━━┥ PROMPT┠━━──

prompt_func()
{
	previous_return_value=$?;

	reset="\[\033[0m\]"
	if [[ $previous_return_value -eq 0 ]] ; then
		statuscolor="\[\033[38;5;51m\]"
	else
		statuscolor="\[\033[31m\]"
	fi

	if [[ "$(hostname -f)" = "pcuxcomp101.jetair.be" ]] ; then
		user=""
	else
		user="───╼${reset} \u ${statuscolor}╾"
	fi

	gitinfo="$(__git_ps1 "%s")"
	if [[ ! -z "${gitinfo}" ]] ; then
		repodir="$(git rev-parse --show-toplevel)"
		repodir="${repodir/#${HOME}/\~}"

		subdir="$(git rev-parse --show-prefix)"

		workdirectory="\[\033[33m${repodir}\] ${statuscolor}╾╼${reset} \[\033[32m\]${gitinfo}${reset} ${statuscolor}╾╼${reset} \[\033[33m\]${subdir}"
	else
		workdirectory="\[\033[33m\w\]"
	fi

	line1="${statuscolor}┌${user}╼${reset} ${workdirectory}"
	line2="${statuscolor}└╼${reset} "

	PS1="\n"
	PS1="${PS1}${line1}\n"
	PS1="${PS1}${line2}"
}

PROMPT_COMMAND=prompt_func
